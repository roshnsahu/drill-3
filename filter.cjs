
function cb(number, index, elemnts){
    return number%2 === 0; 
}

function filter(elements, cb) {

    let even = [];

    for (let index=0; index < elements.length; index++){
        
        if (cb(elements[index], index, elements) === true){
            
            even.push(elements[index])
        }
    }
    return even;
}

module.exports = filter;
