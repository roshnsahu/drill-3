const items = [1, 2, 3, 4, 5, 5];

// console.log(items.includes(6))

function cb(element, targetElement){
    return element === targetElement;
}

function find(array, cb, targetElement) {

    for (let index = 0; index < array.length; index++){
       if( cb(array[index], targetElement)){
            return targetElement
        }
    }
    return undefined;

}

module.exports = find;
console.log(find(items, cb, 5))