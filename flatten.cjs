const nestedArray = [1, [2], [[3]], [[[4]]]];

let flattenedArray = [];

function flatten(elements, depth=1) {

    if(elements === undefined || elements === '') {
        return;
    }
    
    if(depth < 0){
        return elements;
    }


    for(let index = 0; index < elements.length; index++) {
        
        if(depth == 'Infinity' || depth > 0) {
            flatten(elements[index], depth-1);
        } 
        
        if(typeof elements[index] === 'number' || typeof elements[index] === 'string' || depth === 0) {
            flattenedArray.push(elements[index]);
            continue;
        }

        else {
            flattenedArray.push(elements[index]);
            return;
        }
    }

    return flattenedArray;
}

module.exports = flatten;