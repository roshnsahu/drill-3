function iteratee(element, index, arr){
    return element*element
}

function map(elements, iteratee) {
    const squares = [];

    if(!Array.isArray(elements))
        return [];


    else{
    for(let index=0; index < elements.length; index++){
        squares.push(iteratee(elements[index], index, elements))    
        }
    }
    return squares;
}

module.exports = map;