const items = [1, 2, 3, 4, 5, 5];

function cb(accumulator, currentValue, index, arr) {
    return accumulator + currentValue;
}

function reduce(elements, cb, initialValue) {

    if(elements.length === 0) {
        if(initialValue !== undefined) {
            return initialValue;
        }
        return 0;
    }

    let accumulator = initialValue, startIndex = 0;
    
    if(initialValue === undefined) {
        accumulator = elements[0];
        startIndex = 1;
    } 

    for(let index = startIndex; index < elements.length; index++) {
        accumulator = cb(accumulator, elements[index], index, elements);
    }

    return accumulator;
}

module.exports = reduce;
